# Syncing repos
The Manjaro repos are updated at different intervals.

The recommended method is to use cron to schedule a check using one or more scripts.

You can use the sample scripts provided

[sync-stable-repo.sh](sync-stable-repo.sh)
[sync-testing-repo.sh](sync-testing-repo.sh)
[sync-unstable-repo.sh](sync-unstable-repo.sh)

## Stable
The stable repo is updated when security mandates an update and when the core team decides the testing branch is ready for deployment.

Suggested sync interval 8-12 hours

## Testing
Testing repo

Suggested sync interval 4-6 hours

## Unstable
Unstable repo changes often as it is the development branch.

Suggested sync interval 1-2 hours

