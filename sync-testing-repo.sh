#!/bin/sh

# This is a simple mirroring script. To save bandwidth it first checks a
# timestamp via HTTP and only runs rsync when the timestamp differs from the
# local copy. As of 2016, a single rsync run without changes transfers roughly
# 6MiB of data which adds up to roughly 250GiB of traffic per month when rsync
# is run every minute. Performing a simple check via HTTP first can thus save a
# lot of traffic.

# copied from Arch Linux sync script

HOME="/path/to/destination"
TARGET="${HOME}/manjaro"
TMP="${HOME}/.tmp/manjaro"
LOCK="/tmp/rsync-manjaro-testing.lock"

# NOTE: You'll probably want to change this or remove the --bwlimit setting in
# the rsync call below
BWLIMIT=10000

SOURCE="rsync://your.chosen.server/repo/path"

[ ! -d "${TARGET}" ] && mkdir -p "${TARGET}"
[ ! -d "${TMP}" ] && mkdir -p "${TMP}"

exec 9>"${LOCK}"
flock -n 9 || exit

## if we are called without a tty (cronjob) only run when there are changes
#if ! tty -s && diff -b <(curl -s "${STATE}") "${TARGET}/state" >/dev/null; then
#	exit 0
#fi

if ! stty &>/dev/null; then
    QUIET="-q"
fi

# sleep $((RANDOM%60))

# excluded fh 2019-12-16
rsync -rtlvH --safe-links \
    --bwlimit=${BWLIMIT} \
    --delete-after --progress \
    -h ${QUIET} --timeout=600 --contimeout=120 -p \
    --delay-updates --no-motd \
    --temp-dir="${TMP}" \
    --exclude=/stable \
    --exclude=/unstable \
    --exclude=/x32-stable \
    --exclude=/x32-unstable \
    ${SOURCE} \
    "${TARGET}"

